package com.mii.poc.backend.myins.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class MyinsController {

	@CrossOrigin
	@GetMapping("/asuransi_saya")
	public String asuransi_saya() {
// save a single Account
		
		return "{\r\n" + 
				"	\"asuransi_1\":\r\n" + 
				"		{\r\n" + 
				"			\"nama_asuransi\":\"Asuransi Mobil Adira\",\r\n" + 
				"			\"jenis_asuransi\":\"Comperhensive (All Risk)\",\r\n" + 
				"			\"no_plat\":\"B 1234 ABC\",\r\n" + 
				"			\"status\":\"Berhasil/status polis sudah terbit\"\r\n" + 
				"		},\r\n" + 
				"	\"asuransi_2\":\r\n" + 
				"		{\r\n" + 
				"			\"nama_asuransi\":\"PRIMAJAGA 100\",\r\n" + 
				"			\"no_sertifikat\":\"123456890-1234\",\r\n" + 
				"			\"premi\":\"IDR 20.000,00/bulan\",\r\n" + 
				"			\"santunan\":\"IDR 5.000.000,00\",\r\n" + 
				"			\"status\":\"Akan Berakhir dalam 30 hari\"\r\n" + 
				"		}\r\n" + 
				"}";
	}
	
	@CrossOrigin
	@GetMapping("/detail_asuransi")
	public String detail_asuransi() {
// save a single Account
		
		return "{\r\n" + 
				"    \"nama_lengkap\": \"Agus Susanto\",\r\n" + 
				"    \"no_hp\": \"08126677123\",\r\n" + 
				"    \"email\": \" agus.susanto@gmail.com\",\r\n" + 
				"    \"alamat\": null,\r\n" + 
				"    \"no_plat\": \" B 1234 ABC\",\r\n" + 
				"    \"no_mesin\": \" A3R2A0129909\",\r\n" + 
				"    \"no_rangka\": \" MH3SA9910A.1129937\",\r\n" + 
				"    \"sumber_rekening\": \" TABUNGAN DANAMON LEBIH 0035981199203 IDR\",\r\n" + 
				"    \"produk\": \"Total Loss Only\",\r\n" + 
				"    \"periode_asuransi\": \" 1 tahun(12 desember 2020)\",\r\n" + 
				"    \"tahun_kendaraan\": \" 2018\",\r\n" + 
				"    \"merk\": \" Honda JAZZ ALL NEW A I-VTEC 1.5 M/T\",\r\n" + 
				"    \"tambahan\": \" Angin Topan, Banjir, Badai, Hujan Es dan Tanah Longsor\",\r\n" + 
				"    \"kode_wilayah_plat\": \"B - Jakarta\",\r\n" + 
				"    \"aksesoris_tambahan\": \" Kaca film,lainnya\",\r\n" + 
				"    \"harga_total\": \" IDR 598.500,00\",\r\n" + 
				"    \"no_ref\": \"123456789\",\r\n" + 
				"    \"tanggal\": \"31-03-2020 04:31:03\",\r\n" + 
				"    \"response\": \"Pembelian sedang di proses.\",\r\n" + 
				"    \"response_message\": \"Selamat, Anda telah mengirimkan data anda sebagai untuk pemesanan Asuransi Mobil. Tim kami akan menghubungi Anda.\"\r\n" + 
				"}";
	}
	
	
}
